static const char norm_fg[] = "#aedbee";
static const char norm_bg[] = "#0b101a";
static const char norm_border[] = "#7999a6";

static const char sel_fg[] = "#aedbee";
static const char sel_bg[] = "#4C7391";
static const char sel_border[] = "#aedbee";

static const char urg_fg[] = "#aedbee";
static const char urg_bg[] = "#236690";
static const char urg_border[] = "#236690";

static const char *colors[][3]      = {
    /*               fg           bg         border                         */
    [SchemeNorm] = { norm_fg,     norm_bg,   norm_border }, // unfocused wins
    [SchemeSel]  = { sel_fg,      sel_bg,    sel_border },  // the focused win
    [SchemeUrg] =  { urg_fg,      urg_bg,    urg_border },
};
