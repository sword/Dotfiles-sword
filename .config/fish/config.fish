### EXPORT ### 
set fish_greeting                                 # Supresses fish's intro message 
set TERM "xterm-256color"                         # Sets the terminal type
set -e fish_user_paths 
set -U fish_user_paths $HOME/.local/bin $fish_user_paths
set -x -g LC_ALL es_ES.UTF-8
set -x -g LANG es_ES.UTF-8

oh-my-posh --init --shell fish --config ~/.poshthemes/negligible.omp.json | source

# Ensure fisherman and plugins are installed
if not test -f $HOME/.config/fish/functions/fisher.fish
  echo "==> Fisherman not found.  Installing."
  curl -sLo ~/.config/fish/functions/fisher.fish --create-dirs git.io/fisher
  fisher
end

# set rust path
set -gx PATH "$HOME/.cargo/bin/" $PATH

# Functions needed for !! and !$
function __history_previous_command
  switch (commandline -t)
  case "!"
    commandline -t $history[1]; commandline -f repaint
  case "*"
    commandline -i !
  end
end

function __history_previous_command_arguments
  switch (commandline -t)
  case "!"
    commandline -t ""
    commandline -f history-token-search-backward
  case "*"
    commandline -i '$'
  end
end
# Function for creating a backup file
# ex: backup file.txt
# result: copies file as file.txt.bak
function backup --argument filename
    cp $filename $filename.bak
end

# Function for copying files and directories, even recursively.
# ex: copy DIRNAME LOCATIONS
# result: copies the directory and all of its contents.
function copy
    set count (count $argv | tr -d \n)
    if test "$count" = 2; and test -d "$argv[1]"
	set from (echo $argv[1] | trim-right /)
	set to (echo $argv[2])
        command cp -r $from $to
    else
        command cp $argv
    end
end

# Function for printing a column (splits input on whitespace)
# ex: echo 1 2 3 | coln 3
# output: 3
function coln
    while read -l input
        echo $input | awk '{print $'$argv[1]'}'
    end
end

# Function for printing a row
# ex: seq 3 | rown 3
# output: 3
function rown --argument index
    sed -n "$index p"
end

# Function for ignoring the first 'n' lines
# ex: seq 10 | skip 5
# results: prints everything but the first 5 lines
function skip --argument n
    tail +(math 1 + $n)
end

# Function for taking the first 'n' lines
# ex: seq 10 | take 5
# results: prints only the first 5 lines
function take --argument number
    head -$number
end



#Abbreviations Void Linux
abbr -a search xbps-query -Rs
abbr -a install sudo xbps-install -S
abbr -a update sudo xbps-install -Su
abbr -a remove sudo xbps-remove -R 
abbr -a clean sudo xbps-remove -O
abbr -a reconf sudo xbps-reconfigure
abbr -a update-grub sudo grub-mkconfig -o /boot/grub/grub.cfg
abbr -a restricted 'cd /home/jose/Plantillas/void-linux/void-packages && grep -rl '^restricted=' srcpkgs/'
abbr -a services sudo sv status /var/service/*
abbr -a non-free 'xbps-query -Mi --repo=https://alpha.de.repo.voidlinux.org/current/nonfree -s \*'
abbr -a doit 'source ~/.config/fish/config.fish && topgrade && sudo xbps-remove -O && hblock && curl -sL "https://raw.githubusercontent.com/pystardust/ytfzf/master/ytfzf" | sudo tee /usr/local/bin/ytfzf >/dev/null && sudo chmod 755 /usr/local/bin/ytfzf && clear'
abbr -a void  cd /home/jose/Plantillas/void-linux/void-packages
abbr -a mklive cd /home/jose/Plantillas/void-linux/void-mklive
abbr -a ignpa 'echo "ignorepkg=pulseaudio" | sudo tee -a /etc/xbps.d/XX-ignore.conf'
abbr -a etcher 'cd ~/Plantillas/varios/etcher && npm start'
abbr -a goodies 'cd ~/Plantillas/void-linux/void-goodies && git pull'
abbr -a xanmod 'cd ~/Plantillas/void-linux/xanmod/void-packages && git pull'
abbr -a nvoid 'cd ~/Plantillas/void-linux/nvoid && git pull'
abbr -a git-update 'cd ~/Plantillas/void-linux/void-packages && git pull &&  cd ~/Plantillas/void-linux/void-mklive && git pull'
abbr -a ymir cd ~/Plantillas/void-linux/ymir-linux/void-packages
abbr -a ymirp 'cd ~/Plantillas/void-linux/ymir-linux/void-packages && git pull'
abbr -a voidp 'git clone git://github.com/void-linux/void-packages.git && xbpsbb'
abbr -a agar cd ~/Público/AgarimOS/ 

# Git
abbr -a addup git add -u 
abbr -a addall git add . 
abbr -a branch git branch 
abbr -a checkout git checkout
abbr -a clone git clone
abbr -a commit git commit -m 
abbr -a fetch git fetch 
abbr -a pull git pull origin 
abbr -a push git push origin 
abbr -a tag git tag 
abbr -a newtag git tag -a

# Alias's for multiple directory listing commands
abbr -a la ls -Alh # show hidden files
abbr -a ls ls -aFh --color=always # add colors and file type extensions
abbr -a lx ls -lXBh  # sort by extension
abbr -a lk ls -lSrh  # sort by size
abbr -a lc ls -lcrh  # sort by change time
abbr -a lu ls -lurh # sort by access time
abbr -a lr ls -lRh # recursive ls
abbr -a lt ls -ltrh # sort by date
alias lm='ls -alh |more' # pipe through 'more'
abbr -a lw ls -xAh # wide listing format
abbr -a ll ls -Fls # long listing format
abbr -a labc ls -lap  #alphabetical sort
abbr -a lf ls -l | egrep -v '^d' # files only
abbr -a  ldir ls -l | egrep '^d' # directories only

#youtube-dl
abbr -a yta-aac 'youtube-dl --extract-audio --audio-format aac'
abbr -a yta-best 'youtube-dl --extract-audio --audio-format best'
abbr -a yta-flac 'youtube-dl --extract-audio --audio-format flac'
abbr -a yta-m4a 'youtube-dl --extract-audio --audio-format m4a'
abbr -a yta-mp3 'youtube-dl --extract-audio --audio-format mp3'
abbr -a yta-opus 'youtube-dl --extract-audio --audio-format opus'
abbr -a yta-vorbis 'youtube-dl --extract-audio --audio-format vorbis'
abbr -a yta-wav 'youtube-dl --extract-audio --audio-format wav'

abbr ytv-best 'youtube-dl -f bestvideo+bestaudio'

# Corona
alias top10="curl 'https://corona-stats.online?top=10&source=2&minimal=true&emojis=true'"
alias top20="curl 'https://corona-stats.online?top=20&source=2&minimal=true&emojis=true'"
alias top30="curl 'https://corona-stats.online?top=30&source=2&minimal=true&emojis=true'"
alias top40="curl 'https://corona-stats.online?top=40&source=2&minimal=true&emojis=true'"

abbr -a coves curl -L covid19.trackercli.com/es
abbr -a covat curl -L covid19.trackercli.com/at
abbr -a covuk curl -L covid19.trackercli.com/gb
abbr -a covde curl -L covid19.trackercli.com/de
abbr -a covfr curl -L covid19.trackercli.com/fr
abbr -a covit curl -L covid19.trackercli.com/it
abbr -a covbe curl -L covid19.trackercli.com/be
abbr -a covnl curl -L covid19.trackercli.com/nl
abbr -a covsk curl -L covid19.trackercli.com/sk
abbr -a covru curl -L covid19.trackercli.com/ru
abbr -a covpl curl -L covid19.trackercli.com/pl
abbr -a covpt curl -L covid19.trackercli.com/pt
abbr -a covcz curl -L covid19.trackercli.com/cz
abbr -a covus curl -L covid19.trackercli.com/us
abbr -a covhr curl -L covid19.trackercli.com/hr
abbr -a covrs curl -L covid19.trackercli.com/rs
abbr -a covch curl -L covid19.trackercli.com/ch
abbr -a covil curl -L covid19.trackercli.com/il

# I am lazy
abbr -a .. cd ..
abbr -a ... cd ../..
abbr -a .... cd ../../..
abbr -a ..... cd ../../../..
abbr -a ...... cd ../../../../..
abbr -a descargas cd /home/jose/Descargas
abbr -a videos cd /home/jose/Vídeos
abbr -a documentos cd /home/jose/Documentos
abbr -a musica cd /home/jose/Música
abbr -a themes d /usr/share/themes
abbr -a fonts cd /usr/share/fonts
abbr -a icons cd /usr/share/icons
abbr -a trash 'sudo rm -rf ~/.local/share/Trash/*'
abbr -a npmu 'sudo npm upgrade -g npm'
abbr -a nzsh nano ~/.config/fish/config.fish
abbr -a sce source ~/.config/fish/config.fish
abbr -a del rm -rf
abbr -a sdel sudo rm -rf
abbr -a ft fc-cache -f -v
abbr -a kzsh kate ~/.config/fish/config.fish
abbr -a probe 'sudo -E hw-probe -all -upload'
abbr -a fzsh featherpad ~/.config/fish/config.fish
abbr -a npmaf 'npm audit fix'
abbr -a ter sensors 
abbr -a diff colordiff
abbr -a untar tar -zxvf
abbr -a multitail multitail --no-repeat -c
abbr -a loc locate 
abbr -a lca lsd -lA
abbr -a vi vim
abbr -a mount mount |column -t
abbr -a chshb chsh -s /bin/bash
abbr -a chshz chsh -s /bin/zsh
abbr -a userlist cut -d: -f1 /etc/passwd
abbr -a bat upower -i /org/freedesktop/UPower/devices/battery_BAT0| grep --color=never -E "state|to\ full|percentage"
abbr -a inf kinfocenter 
abbr -a cl clear
abbr -a who whoami
abbr -a cc sudo sh -c "sync; echo 3 > /proc/sys/vm/drop_caches" # Clear System Cache
abbr -a spt speedtest-cli
abbr -a wifi wirelessNetworksInRange

#Inxi
abbr -a mac inxi -zv8
abbr -a weather inxi -wxxx
abbr -a  machine inxi -Fxxxrza

# Kitty
abbr -a icat kitty +kitten icat
abbr -a d kitty +kitten diff
abbr -a clip kitty +kitten clipboard
abbr -a kittyc nano ~/.config/kitty/kitty.conf
abbr -a kittys nano ~/.config/kitty/settings.conf
abbr -a kittyf nano ~/.config/kitty/font.conf
abbr -a kittyk nano ~/.config/kitty/keybindings.conf
abbr -a kittyb nano ~/.config/kitty/bell.conf
abbr -a kittycu nano ~/.config/kitty/cursor.conf
abbr -a kittym nano ~/.config/kitty/mouse.conf
abbr -a kittyp nano ~/.config/kitty/performance.conf
abbr -a kittysc nano ~/.config/kitty/scrollback.conf
abbr -a kittyt nano ~/.config/kitty/tab.conf
abbr -a kittyw nano ~/.config/kitty/window.conf

#Ricing
abbr -a nerdfetcho 'curl -fsSL https://raw.githubusercontent.com/ThatOneCalculator/NerdFetch/master/nerdfetch | sh'
abbr -a pipesa cpipes -p30 -r1
abbr -a pipesb cpipes -p100 -r0 -i1
abbr -a pman colorscript -e 30
abbr -a skull colorscript -e 33
abbr -a spaceinvaders colorscript -e 38
abbr -a clock tty-clock -c

#Ytfzf
abbr -a showme ytfzf -t
abbr -a ytfzfi 'curl -sL "https://raw.githubusercontent.com/pystardust/ytfzf/master/ytfzf" | sudo tee /usr/local/bin/ytfzf >/dev/null && sudo chmod 755 /usr/local/bin/ytfzf'

#Common mistakes

abbr -a cd.. cd ..
abbr -a pdw pwd
abbr a isntall install

#Other aliases

# reboot / halt / poweroff
abbr -a reboot sudo /sbin/reboot
abbr -a poweroff sudo /sbin/poweroff
abbr -a halt sudo /sbin/halt
abbr -a shutdown sudo /sbin/shutdown

# alias chmod commands
abbr -a mx chmod a+x
abbr -a 000 chmod -R 000
abbr -a 644 chmod -R 644
abbr -a 666 chmod -R 666
abbr -a 755 chmod -R 755
abbr -a 777 chmod -R 777

# Search command line history
abbr -a h 'history | grep'
abbr -a tree tree -CAhF --dirsfirst
abbr -a treed tree -CAFd
abbr -a mountedinfo df -hT

# Remove a directory and all files
abbr -a rmd /bin/rm  --recursive --force --verbose

# Stop after sending count ECHO_REQUEST packets #
abbr -a ping ping -c 5

# Do not wait interval 1 second, go fast #
abbr -a fastping ping -c 100 -s.2

## shortcut  for iptables and pass it via sudo#
abbr -a ipt sudo /sbin/iptables
 
# display all rules #
abbr -a iptlist sudo /sbin/iptables -L -n -v --line-numbers
abbr -a iptlistin sudo /sbin/iptables -L INPUT -n -v --line-numbers
abbr -a iptlistout sudo /sbin/iptables -L OUTPUT -n -v --line-numbers
abbr -a iptlistfw sudo /sbin/iptables -L FORWARD -n -v --line-numbers
abbr -a firewall iptlist

# do not delete / or prompt if deleting more than 3 files at a time #
abbr -a rm rm -I --preserve-root
 
# get web server headers #
abbr -a header curl -I 
 
# find out if remote server supports gzip / mod_deflate or not #
abbr -a headerc curl -I --compress

# confirmation #
abbr -a mv mv -i
abbr -a cp cp -i
abbr -a ln ln -i

# Parenting changing perms on / #
abbr -a chown chown --preserve-root
abbr -a chmod chmod --preserve-root
abbr -a chgrp chgrp --preserve-root

## pass options to free ##
abbr -a meminfo free -m -l -t

## get top process eating memory
abbr -a psmem 'ps auxf | sort -nr -k 4'
abbr -a psmem10 'ps auxf | sort -nr -k 4 | head -10'

## get top process eating cpu ##
abbr -a pscpu 'ps auxf | sort -nr -k 3'
abbr -a pscpu10 'ps auxf | sort -nr -k 3 | head -10'

## Get server cpu info ##
abbr -a cpuinfo lscpu
 
## get GPU ram on desktop / laptop##
abbr -a gpumeminfo 'grep -i --color memory /var/log/Xorg.0.log'

# become root #
abbr -a root sudo -i
abbr -a su sudo -i

# Time 
abbr -a now 'date +"%T"'
abbr -a nowtime now
abbr -a nowdate 'date +"%d-%m-%Y"'

# make directory and any parent directories needed
abbr -a mkdir mkdir -p
abbr -a smkdir sudo mkdir -p

# Give less options to man
set MANPAGER less -s -M +Gg

## this one saved by butt so many times ##
abbr -a wget wget -c
abbr -a path 'echo -e ${PATH//:/\\n}'

# make common commands easier to read for humans
abbr -a df df -Tha --total
abbr -a du du -ach | sort -h
abbr -a free free -mth

# custom cmatrix
abbr -a cmatrix cmatrix -bC blue

# search processes (find PID easily)
abbr -a psg 'ps aux | grep -v grep | grep -i -e VSZ -e'

# show all processes
abbr -a psf ps auxfww

# Colorize grep output (good for log files)
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

#curl -fsSL https://raw.githubusercontent.com/ThatOneCalculator/NerdFetch/master/nerdfetch | sh
nerdfetch
